package com.qaagility.controller;

public class Count {

    public int division(int numerator, int denominator) {
        if (denominator == 0){
            return Integer.MAX_VALUE;
        }
        else{
            return numerator / denominator;
        }
    }

}
