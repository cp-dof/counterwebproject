package com.qaagility.controller;

import static org.mockito.Mockito.*;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.ui.ModelMap;


public class AboutTest {

	@Test
	public void testDesc() throws Exception {
        String k= new About().desc();
        assertTrue("About Description", k.contains("This application was copied from somewhere, sorry but I cannot give the details and the proper copyright notice. Please don't tell the police!"));
	}
}
